
default:all

tatti_1: tatti_1.c
	gcc tatti_1.c -o tatti_1

tatti_2: tatti_2.c
	gcc tatti_2.c -o tatti_2

tatti_3: tatti_3.c
	gcc tatti_3.c -o tatti_3

clean:
	rm tatti_1 tatti_2 tatti_3

save_temps:
	gcc tatti_1.c -o tatti_1 --save-temps

clean_temps:
	rm *.{i,s,o}
